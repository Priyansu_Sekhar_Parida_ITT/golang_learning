package main

import (
	"fmt"
	"sync"
)

var z = 0

func print(wg *sync.WaitGroup, ch chan bool) {
	ch <- true //sending data to the channel
	z = z + 1
	fmt.Println(x)
	<-ch      //receiving data from the channel.
	wg.Done() //decrement the counter
}
func main() {
	var w sync.WaitGroup     // wait for goroutine to finish execution.
	ch := make(chan bool, 1) //making channel capacity as 1
	for i := 0; i < 100; i++ {
		w.Add(1) //to increment the counter
		go print(&w, ch)
	}
	w.Wait() //wait till counter becomes zero.
}
