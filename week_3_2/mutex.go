package main

import (
	"fmt"
	"sync"
)

var x = 0

func increment(wg *sync.WaitGroup, m *sync.Mutex) {
	m.Lock()
	x = x + 1
	fmt.Println(x)
	m.Unlock()
	wg.Done() //Decrement counter
}
func main() {
	var w sync.WaitGroup
	var m sync.Mutex
	for i := 0; i < 100; i++ {
		w.Add(1) //Increment counter
		go increment(&w, &m)
	}
	w.Wait() //wait till counter becomes zero.
}
