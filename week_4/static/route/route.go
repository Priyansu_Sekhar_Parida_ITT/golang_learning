package route

import (
	"html/template"
	"net/http"
)

func Index(writer http.ResponseWriter, request *http.Request) {
	t, _ := template.ParseFiles("index.html")
	t.Execute(writer, "To the e-commerce app")
}
