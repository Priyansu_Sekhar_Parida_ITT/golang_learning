package main

import "fmt"

type value struct {
	number int
}

func (x *value) print() {
	fmt.Println("The value after incrementing:", x.number+1)
}

func main() {
	var x value
	fmt.Print("Enter a number:")
	fmt.Scanf("%d\n", &x.number)
	x.print() // calling pointer receiver with value
}
