package main

import "fmt"

type increment struct {
	number int
}

func (p increment) print() {
	fmt.Println("The value after incrementing:", p.number+1)
}

func main() {
	var x increment
	fmt.Print("Enter a number:")
	fmt.Scanf("%d\n", &x.number)
	p := &x
	p.print() // calling value receiver with pointer

}
