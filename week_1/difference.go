package main

import "fmt"

func main() {
	var n1, n2, n3, n4, n5, n6 float64
	var max, min float64
	fmt.Println("Enter 6 real numbers:")
	fmt.Scanf("%f %f %f %f %f %f", &n1, &n2, &n3, &n4, &n5, &n6)
	//Comparing numbers
	if n1 >= n2 && n1 >= n3 && n1 >= n4 && n1 >= n5 && n1 >= n6 {
		max = n1
	} else if n2 >= n1 && n2 >= n3 && n2 >= n4 && n2 >= n5 && n2 >= n6 {
		max = n2
	} else if n3 >= n1 && n3 >= n2 && n3 >= n4 && n3 >= n5 && n3 >= n6 {
		max = n3
	} else if n4 >= n1 && n4 >= n2 && n4 >= n3 && n4 >= n5 && n4 >= n6 {
		max = n4
	} else if n5 >= n1 && n5 >= n2 && n5 >= n3 && n5 >= n4 && n5 >= n6 {
		max = n5
	} else {
		max = n6
	}

	if n1 <= n2 && n1 <= n3 && n1 <= n4 && n1 <= n5 && n1 <= n6 {
		min = n1
	} else if n2 <= n1 && n2 <= n3 && n2 <= n4 && n2 <= n5 && n2 <= n6 {
		min = n2
	} else if n3 <= n1 && n3 <= n2 && n3 <= n4 && n3 <= n5 && n3 <= n6 {
		min = n3
	} else if n4 <= n1 && n4 <= n2 && n4 <= n3 && n4 <= n5 && n4 <= n6 {
		min = n4
	} else if n5 <= n1 && n5 <= n2 && n5 <= n3 && n5 <= n4 && n5 <= n6 {
		min = n5
	} else {
		min = n6
	}
	fmt.Println("Difference of the maximum and minimum values of these numbers is:", max-min)
}
