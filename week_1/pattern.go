package main

import "fmt"

func main() {
	var i, j, k int
	for i = 1; i <= 4; i++ {
		k = i
		for j = 1; j <= 7; j++ {
			if j >= (5-i) && j <= (3+i) {
				fmt.Print(k) //taken k variable for printing numbers
				if j < 4 {
					k--
				} else {
					k++
				}
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Print("\n")
	}
}
