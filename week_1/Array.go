package main

import "fmt"

func swapelement(arr []float64, pos1 int, pos2 int) {
	var temp float64
	temp = arr[pos1]
	arr[pos1] = arr[pos2]
	arr[pos2] = temp
}
func main() {
	var size, i, j int
	var pos2 int
	var temp float64
	fmt.Println("Enter size of the array:")
	fmt.Scanf("%d\n", &size)
	arr := make([]float64, size)
	fmt.Println("Enter the array elements: ")
	for i = 0; i < size; i++ {
		fmt.Scanf("%f\n", &arr[i])
	}
	var total, avg float64 = 0, 0
	for i = 0; i < size; i++ {
		total += arr[i]
	}
	avg = total / float64(size) //Calculating average of numbers
	fmt.Println("The average of array elements is:", avg)
	for i = 0; i < size; i++ {
		for j = i + 1; j < size; j++ {
			if arr[i] > arr[j] {
				temp = arr[i]
				arr[i] = arr[j]
				arr[j] = temp
			}
		}
	}
	fmt.Println("Sorting an array :")
	for i = 0; i < size; i++ {
		fmt.Println(arr[i])
	}
	fmt.Print("Enter the first index want to swap: ")
	var pos1 int
	fmt.Scanf("%d\n", &pos1)
	fmt.Print("Enter the second index want to swap: ")
	fmt.Scanf("%d\n", &pos2)
	swapelement(arr, pos1, pos2)
	fmt.Println("After swapping an element")
	for i = 0; i < size; i++ {
		fmt.Println(arr[i])
	}
}
