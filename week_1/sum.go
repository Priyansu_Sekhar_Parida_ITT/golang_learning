package main

import "fmt"

func main() {
	var n1, lastdigit int
	sum := 0
	fmt.Print("Enter a number:")
	fmt.Scanf("%d", &n1)
	for ; n1 > 0; n1 = n1 / 10 {
		lastdigit = n1 % 10
		sum += lastdigit
	}
	fmt.Println("sum of digits of given number:", sum)
}
