package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	f, err := os.Create("NOTES.TEXT") //Creating Text file
	if err != nil {
		fmt.Printf("error creating file: %v", err)
		return
	}
	defer f.Close()
	for i := 1; i <= 100; i++ {
		_, err := f.WriteString(fmt.Sprintf("%d\n", i))
		if err != nil {
			fmt.Printf("error writing string: %v", err)
		}
	}
	data, err := ioutil.ReadFile("D:/go/src/Golang_Learning/week_1/NOTES.TEXT") //Reading from file
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	fmt.Println("Contents of file:")
	fmt.Println(string(data))
}
