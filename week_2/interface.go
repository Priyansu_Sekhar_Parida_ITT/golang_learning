package main

import (
	"fmt"
)

type SalaryCalculator interface {
	CalculateSalary() int
}

type Permanent struct {
	empId    int
	basicpay int
	pf       int
}

type Contract struct {
	empId    int
	basicpay int
}

func (p Permanent) CalculateSalary() int {
	return p.basicpay + p.pf
}

func (c Contract) CalculateSalary() int {
	return c.basicpay
}

func totalExpense(s []SalaryCalculator) {
	expense := 0
	for _, v := range s {
		expense = expense + v.CalculateSalary()
	}
	fmt.Printf("Total Expense Per Month $%d", expense)
}

func main() {
	var pemp1 Permanent
	fmt.Print("Enter Permanent empId:")
	fmt.Scanf("%d\n", &pemp1.empId)
	fmt.Print("Enter basicpay:")
	fmt.Scanf("%d\n", &pemp1.basicpay)
	fmt.Print("Enter pf:")
	fmt.Scanf("%d\n", &pemp1.pf)
	fmt.Println()

	var cemp1 Contract
	fmt.Print("Enter Contract empId:")
	fmt.Scanf("%d\n", &cemp1.empId)
	fmt.Print("Enter basicpay:")
	fmt.Scanf("%d\n", &cemp1.basicpay)
	fmt.Println()

	employees := []SalaryCalculator{pemp1, cemp1}
	totalExpense(employees)
}
