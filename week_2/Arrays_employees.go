package main

import (
	"fmt"
)

type Employee struct {
	name   string
	id     int
	salary int
}

var employees [5]Employee

func main() {
	fmt.Println("Enter Details of all Employees:")
	for i := 0; i < 5; i++ {
		fmt.Printf("Employee %d:- \n", i+1)
		fmt.Print("Name:")
		fmt.Scanf("%s\n", &employees[i].name)
		fmt.Print("Id: ")
		fmt.Scanf("%d\n", &employees[i].id)
		fmt.Print("Salary: ")
		fmt.Scanf("%d\n", &employees[i].salary)
		fmt.Println()
	}
	fmt.Println("-------------- All Employees Details ---------------")
	for i := 0; i < 5; i++ {
		fmt.Print("Name: ")
		fmt.Printf("%s\n", employees[i].name)
		fmt.Print("Id: ")
		fmt.Printf("%d\n:", employees[i].id)
		fmt.Print("Salary: ")
		fmt.Printf("%d\n", employees[i].salary)
		fmt.Println()
	}
}
