package main

import (
	"calculator/calculation"
	"fmt"
)

func main() {
	var number1, number2 float64
	fmt.Print("Enter number1:")
	fmt.Scanf("%f\n", &number1)
	fmt.Print("Enter number2:")
	fmt.Scanf("%f\n", &number2)
	fmt.Println("Addition of two numbers is:", calculation.Add(number1, number2))
	fmt.Println("Substraction of two numbers is:", calculation.Substract(number1, number2))
	fmt.Println("Multiplication of two numbers is:", calculation.Multiply(number1, number2))
	fmt.Println("Dividing of two numbers is:", calculation.Divide(number1, number2))
}
