package calculation

func Add(n1, n2 float64) float64 {
	result := n1 + n2
	return result
}

func Substract(n1, n2 float64) float64 {
	return n1 - n2
}

func Multiply(n1, n2 float64) float64 {
	return n1 * n2
}

func Divide(n1, n2 float64) float64 {
	return n1 / n2
}
