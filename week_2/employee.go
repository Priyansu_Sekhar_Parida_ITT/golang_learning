package main

import (
	"fmt"
)

type employee struct {
	name   string
	id     int
	salary int
}

func main() {
	var n int
	fmt.Print("Enter no. of employee details:")
	fmt.Scanf("%d\n", &n)
	employees := make([]employee, n) //slice of emplyees
	fmt.Printf("Enter %d Employee Details\n", n)
	for i := 0; i < n; i++ {
		fmt.Printf("Employee %d:- \n", i+1)
		fmt.Print("Name:")
		fmt.Scanf("%s\n", &employees[i].name)
		fmt.Print("Id: ")
		fmt.Scanf("%d\n", &employees[i].id)
		fmt.Print("Salary: ")
		fmt.Scanf("%d\n", &employees[i].salary)
		fmt.Println()
	}
	fmt.Println("-------------- All Employees Details ---------------")
	for i := 0; i < n; i++ {
		fmt.Print("Name: ")
		fmt.Printf("%s\n", employees[i].name)
		fmt.Print("Id: ")
		fmt.Printf("%d\n:", employees[i].id)
		fmt.Print("Salary: ")
		fmt.Printf("%d\n", employees[i].salary)
		fmt.Println()
	}
}
